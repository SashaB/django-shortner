from django.contrib import admin

from .models import ShortURL


class ShortURLAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Shorted url', {'fields': ['owner', 'short_url', 'original_url', 'views_counter']})
    ]
    list_display = ('owner', 'short_url', 'original_url', 'views_counter')
    list_filter = ('owner', 'views_counter')
    search_fields = ('owner', 'original_url')

admin.site.register(ShortURL, ShortURLAdmin)
