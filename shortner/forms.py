from django import forms

from .models import ShortURL


class ShortURLForm(forms.ModelForm):

    class Meta:
        model = ShortURL
        fields = ('original_url',)

    def clean_original_url(self):
        url = self.cleaned_data['original_url']
        if not url.startswith('http'):
            raise forms.ValidationError('The given URL must start with http or https')

        return url

