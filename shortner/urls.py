from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^(?P<url_token>[-\w]{2,})$', views.redirect_url, name='redirect_short_url'),
    url(r'^!(?P<url_token>[-\w]{2,})$', views.short_url_details, name='short_url_details'),
]
