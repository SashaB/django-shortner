from django.db import models
from django.core.urlresolvers import reverse
from django.conf import settings
from django.contrib.auth.models import User


class ShortURL(models.Model):
    owner = models.ForeignKey(User, related_name='user_urls', null=True, blank=True)
    short_url = models.CharField(max_length=settings.SHORT_URL_LENGTH_BOUNDS[1], null=True, blank=True, unique=True)
    original_url = models.CharField(max_length=255, null=False, blank=False)
    views_counter = models.IntegerField(default=0, null=False, blank=False)

    def get_absolute_url(self):
        return reverse('shortner:redirect_short_url', kwargs={'url_token': self.short_url})

