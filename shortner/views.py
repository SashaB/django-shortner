from django.shortcuts import render, get_object_or_404, redirect, reverse
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings
from django.contrib.auth.models import User

from hashids import Hashids

from .models import ShortURL
from .forms import ShortURLForm


def home(request):
    if request.method == 'POST':
        form = ShortURLForm(data=request.POST)
        if form.is_valid():
            try:
                short_link = ShortURL.objects.get(original_url=request.POST['original_url'])
            except ObjectDoesNotExist:
                random_user = User.objects.all().order_by('?')[1]
                hashids = Hashids(
                    min_length=settings.SHORT_URL_LENGTH_BOUNDS[0],
                    salt=random_user.email
                )

                short_link = form.save()
                short_link.short_url = hashids.encode(short_link.id)[:settings.SHORT_URL_LENGTH_BOUNDS[1]]
                short_link.owner = random_user
                short_link.save()

            return redirect(short_link.get_absolute_url())
    elif request.method == 'GET':
        form = ShortURLForm()

    return render(request, 'shortner/home.html', {'form': form})


def redirect_url(request, url_token=None):

    shorten_url = get_object_or_404(ShortURL, short_url=url_token)
    shorten_url.views_counter += 1
    shorten_url.save()

    return redirect(shorten_url.original_url)


def short_url_details(request, url_token=None):

    shorten_url = get_object_or_404(ShortURL, short_url=url_token)

    return render(request, 'shortner/url_details.html', {'shorten_url': shorten_url})
