from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User

import requests
from datetime import datetime
import pytz


class Command(BaseCommand):
    help = 'Create fake users'

    # api url for creating fake users
    api_url = 'https://randomuser.me/api/'

    def add_arguments(self, parser):
        parser.add_argument('user_count', nargs='+', type=int)

    def handle(self, *args, **options):
        # number of users to create
        user_count = options['user_count']

        # request api for fake users
        response = requests.get(self.api_url, params={'results': user_count})
        fake_users = response.json()

        user_counter = 1
        for fake_user in fake_users['results']:

            username = fake_user['login']['username']
            password = fake_user['login']['password']
            email = fake_user['email']
            first_name = fake_user['name']['first'].capitalize()
            last_name = fake_user['name']['last'].capitalize()

            # string date to datetime
            naive_date_joined = datetime.strptime(fake_user['registered'], '%Y-%m-%d %H:%M:%S')
            date_joined = pytz.utc.localize(naive_date_joined)

            # create new user
            new_user = User.objects.create_user(
                username=username,
                email=email,
                first_name=first_name,
                last_name=last_name,
                date_joined=date_joined,
                )
            # set user password
            new_user.set_password(password)
            new_user.save()

            print('User {0}'.format(user_counter))
            print('Username: {0}'.format(username))
            print('Password: {0}'.format(password))
            print('Name: {0}'.format(first_name))
            print('Surname: {0}'.format(last_name))
            print('Email: {0}'.format(email))
            print('Joined date: {0}'.format(date_joined))
            print('\n')
            user_counter += 1

        self.stdout.write(self.style.SUCCESS('Users created successfully'))
